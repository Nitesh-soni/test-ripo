<?php

namespace App\Http\Controllers;
use Auth;
class Middleware
{   
    const IS_LOGGED_IN = 'is_logged_in';
    
    public function __construct($request){
        $this->request = $request ;
        $this->errors = [];
    }
    public function is_logged_in(){
        $this->errors[__FUNCTION__] = "Not Logged in";
        return  Auth::check();
    }   
}
