<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session ;
use App\Task ;
class TaskController extends Controller
{
    public function createTask(Request $request){
        Session::driver()->setId($request->input('secret'));
        Session::driver()->start();
        if(Auth::check()){
          Task::create([
                'title' => $request->input('title'),
                'discription' => $request->input('discription'),
                'date' => $request->input('date'),
                'author_id'=> Auth::id(),
            ]);
            return 1;
        }
        else{
            return 0;
        }
    }

    public function fetchUserTask(Request $request){
        Session::driver()->setId($request->input('secret'));
        Session::driver()->start();
        $id = Auth::id();
        $data = DB::table('tasks')->orderBy('date')->where('author_id', $id)->get();
        error_log(print_r($data,true));
        return $data ;
    }
}
