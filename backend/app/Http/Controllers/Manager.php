<?php

namespace App\Http\Controllers;
use App\User ;
use App\Http\Controllers\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\RequestBody;

class Manager
{  
   private function authorize($entity, $action, $reqBody, $respBody){
      $checks = $entity::$middleware[$action] ?? [];
      $middleware = new Middleware($reqBody);
      foreach ($checks as $check){
         $result = $middleware->{$check}();
         if($result === false)
         throw new \Exception($middleware->errors[$check]);
      }
      return true;
   }
   private function validate($entity, $action, $reqBody, $respBody){
      $fields = $entity::$validate[$action] ?? [];
      $rules = [];
      foreach ($fields as $field) {
         $rules[$field] = $entity::$rules[$field];
      }
      if (count($rules)) {
         $validator = Validator::make($reqBody->payload, $rules);
         if ($validator->fails()) {
            $errors = $validator->errors();
            // throw exception 
            $validation = [];
            foreach ($fields as $field) {
               $validation[$field] = $errors->get($field);
               if($validation[$field])
               {
               throw new \Exception($validation[$field][0]);
               }
            }    
         }
      }
   }
   public function handle(Request $request){
         try{
            $respBody = new ResponseBody($request);
            $reqBody = new RequestBody($request);
            $action = $request->action;
            $entityClass =
            "App\\Http\\Controllers\\entities\\". ucfirst($request->namespace);
            if (!class_exists($entityClass, 1)) {
               throw new \Exception("No Such Namespace ($entityClass) available");
             }
            $entity = new $entityClass();
            if (!method_exists($entity, $action)) {
               throw new \Exception("No Such Action ($action) available");
             }
         
            $this->authorize($entity, $action, $reqBody, $respBody); 
            $this->validate($entity, $action, $reqBody, $respBody);
            $respBody = $entity->{$action}($reqBody, $respBody);
            return response()->json($respBody);
         }
         catch (\Exception $e) {
            $respBody->setError($e);
            return response()->json($respBody);
         }
   }
}
