<?php

namespace App\Http\Controllers;
use App\User ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Auth;
use Session ;
class UserController extends Controller
{
        public function register(Request $request){
            $validator = Validator::make($request->all(), [
                'email' => 'required|unique:users',
                'name' => 'required',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return $validator->errors();
            }
                User::create([
                    'name' => $request->input('name'),
                    'email' =>$request->input('email'),
                    'password' => Hash::make($request->input('password')),
                ]);
            return 1;  
        }

        public function login(Request $request){
            try {
                $userdata = array(
                    'email'     => $request->input('email'),
                    'password'  =>$request->input('password')
                );
                if(!Auth::attempt($userdata)){
                    throw new \Exception ('Incorrect email or password ');
                }
                else{
                    $secret = Session::getId();
                    return $secret ;
                }
            } catch (Exception $e) {
                return $e;
            }
            
        }
}
