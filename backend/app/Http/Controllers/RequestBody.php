<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\User;
use Auth ;
use Illuminate\Contracts\Auth\Authenticatable ;
class RequestBody
{
    public $payload;
    public $req;
    public $namespace;
    public $action;
    public $data;
    public function __construct(Request $request)
    {
        $this->namespace = $request->get('namespace');
        $this->action = $request->get('action');
        $this->payload = $request->get('payload');  
        $auth = $request->get('auth');
        if ($auth) {
            try {    
                $value  = $auth['user_id'] . $auth['timestamp'];
                $user = User::findOrFail($auth['user_id']);
                
                $value = $value . $user->secret;          
                if (!(md5($value) == $auth['hash']) ) {
                    throw new \Exception ('Not a valid credentials');
                }
                //let login user in system
                Auth::login($user);
                
            } catch (ModelNotFoundException $e) {
                $err = new ApiException('User Not Found.', 404);
                throw $err;
            }
        } 
    }
}
