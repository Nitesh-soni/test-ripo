<?php
namespace App\Http\Controllers;
use ErrorException;
use App;

class ResponseBody
{
    public $status = 500;
    public $error = null;
    public $data;
    public $request;

    public function __construct($request = null)
    {
        $this->request = $request ?? new \stdClass();
        $this->data = new \stdClass();
        $this->error = new \stdClass();
        $this->error->message = '';
        $this->error->title = 'Error !';
        $this->error->remedy = 'Please Retry or Report to Support Team !';
        $this->error->validation = [];
    }
    public function setError(\Exception $e)
    {   
        $this->error->title = $e->title ?? 'Error !';
        $this->error->message = $e->getMessage();
        $this->error->remedy = $e->remedy ?? '';
        $this->error->validation = $e->validation ?? [];
    }
    public function setData($key, $value)
    {
        $this->data->{$key} = $value;
        $this->error = null;
    }
    public function setStatus($status)
    {
        $this->status = $status;
    }

}
