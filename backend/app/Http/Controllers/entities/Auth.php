<?php
namespace App\Http\Controllers\Entities;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\RequestBody;
use App\Http\Controllers\ResponseBody;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\User;
use  Auth as Authority;
use Illuminate\Support\Str;

class Auth
{
    const IN_EMAIL = 'email';
    const IN_NAME = 'name';
    const IN_PASSWORD = 'password';
    public static $rules = [
        self::IN_EMAIL => 'required|email|max:255',
        self::IN_NAME => 'required',
        self::IN_PASSWORD => 'required',
    ];

    public static $validate = [
        'create' => [ self::IN_NAME ,self::IN_EMAIL , self::IN_PASSWORD],
        'login'  => [ self::IN_EMAIL , self::IN_PASSWORD],
    ];
    public function create(RequestBody $requestBody, ResponseBody $responseBody){
        try{    
            $email = $requestBody->payload['email'];
            // user must exist before.
            $person = User::where('email',$email)->get()->first();
            if($person){
                throw new  \Exception("Email id is already exists");
            }
             $user = new User();
             $requestBody->payload['password'] = Hash::make($requestBody->payload['password']);
             $user = $user->fill($requestBody->payload)->save();
             $responseBody->setStatus(200); 
             $message = "Register Successfully";
             $responseBody->setData('message', $message);
             return $responseBody;
        }
        catch (\Exception $e) {
            $responseBody->setError($e);
            return $responseBody;
        }
                 
    }

    public function login(RequestBody $requestBody, ResponseBody $responseBody){
        try { 
            $user = User::where('email',$requestBody->payload['email'])->firstOrFail();;
            if(!(Hash::check($requestBody->payload['password'], $user->password))){
                throw new \Exception ('Password not match');
            }
            $secret = Str::random();
            $user->secret = $secret;
            $user->save(); 
            $responseBody->setStatus(200); 
            $message = "Login Successfully";
            $responseBody->setData('message', $message);
            $responseBody->setData('user',$user);
            return $responseBody;
        } 
        catch (ModelNotFoundException $e) {
            throw new \Exception ('Email id is not registred');
        }catch (Exception $e) {
            $responseBody->setError($e);
            return $responseBody;
        }             
    }

    public function logout(RequestBody $requestBody, ResponseBody $responseBody){
        try{
            Authority::logout();
            $responseBody->setStatus(200); 
            $message = "Logout Succefully";
            $responseBody->setData('message', $message);
            return $responseBody;
        }
        catch (Exception $e) {
            $responseBody->setError($e);
            return $responseBody;
        }         
    }

    
}
