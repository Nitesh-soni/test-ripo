<?php
namespace App\Http\Controllers\Entities;
use  Auth;
use App\User ;
use App\Task as TaskModel;
use App\Http\Controllers\Middleware;
use App\Http\Controllers\RequestBody;
use App\Http\Controllers\ResponseBody;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Task
{
    const IN_TITLE = 'title';
    const IN_DISCRIPTION = 'discription';
    const IN_DATE = 'date';
    public static $rules = [
        self::IN_TITLE => 'required|max:255',
        self::IN_DISCRIPTION => 'required',
        self::IN_DATE => 'required',
    ];

    public static $validate = [
        'create' => [ self::IN_TITLE ,self::IN_DISCRIPTION , self::IN_DATE],
        'update' => [ self::IN_TITLE ,self::IN_DISCRIPTION , self::IN_DATE]
    ];

    public static $middleware = [
        'list'   => [Middleware::IS_LOGGED_IN],
        'create' => [Middleware::IS_LOGGED_IN],
        'update' => [Middleware::IS_LOGGED_IN],
        'delete' => [Middleware::IS_LOGGED_IN],
    ];

    public function create(RequestBody $requestBody, ResponseBody $responseBody){
        try{   
            $logged_in_user = Auth::user(); 
            $payload = $requestBody->payload ;
            $payload['author_id'] = $logged_in_user->id;
            $task = new TaskModel();
            $task = $task->fill($payload)->save();
            $tasks = $logged_in_user->tasks; 
            $responseBody->setData('tasks',$tasks);
            $responseBody->setStatus(200); 
            return $responseBody;
        }
        catch (\Exception $e) {
            $responseBody->setError($e);
            return $responseBody;
        }             
    }

    public function update(RequestBody $requestBody, ResponseBody $responseBody){
        try{    
            $task =  TaskModel::where('id',$requestBody->payload['id'])->update($requestBody->payload);
            $tasks = Auth::user()->tasks; 
            $responseBody->setData('tasks',$tasks);
            $responseBody->setStatus(200); 
            return $responseBody;
       }
       catch (\Exception $e) {
           $responseBody->setError($e);
           return $responseBody;
       }

    }

    public function list(RequestBody $requestBody, ResponseBody $responseBody){
        try{
            $tasks =  Auth::user()->tasks;
            
            $responseBody->setStatus(200); 
            $responseBody->setData('tasks',$tasks);
            return $responseBody;  
        }
        catch (\Exception $e) {
            $responseBody->setError($e);
            return $responseBody;
        }      
    }

    public function delete(RequestBody $requestBody, ResponseBody $responseBody){
        try{
            TaskModel::findOrFail($requestBody->payload['id'])->delete();
            $responseBody->setStatus(200); 
            $responseBody->setData('message','Delete successfully !!');
            return $responseBody;
        }
        catch (ModelNotFoundException $e) {
            throw new \Exception ('Task Not Found');
        }
        catch(\Exception $e){
            $responseBody->setError($e);
            return $responseBody ;
        }
    }
}