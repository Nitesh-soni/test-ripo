import Vue from 'vue'
import VueRouter from 'vue-router'
import paths from './paths'
import store from '../store'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: '/',
  linkActiveClass: 'active',
  routes: paths
})

router.beforeEach ((to, from, next) => {  
  if ((!store.getters['user/isLoggedIn']) && to.path.startsWith ('/task')) {
    
    return next ({path: '/' ,query: {redirect: to.fullPath},});
  }
  next ()
})

export default router
