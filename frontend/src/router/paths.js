import home from '../views/Home.vue'
import about from '../views/About.vue'
import list from '../pages/task/List.vue'
import SaveTask from '../pages/task/SaveTask'
import item from '../pages/task/Item'
export default [
    {
        path: '/',
        name: 'home',
        component: home
    },
    {
        path: '/about',
        name: 'about',
        component: about
    },  
    {
        path: '/task/list' ,
        name: 'list' ,
        component: list
    },
    {
        path: '/task/item/:task_id' ,
        name: 'item' ,
        component: item
    },
    {
        path: '/task/create' ,
        name: 'CreateTask' ,
        component: SaveTask
    },
    {
        path: '/task/edit/:task_id' ,
        name: 'EditTask' ,
        component: SaveTask
    }
]
