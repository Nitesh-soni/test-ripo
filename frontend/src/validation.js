export default {
    required: value => !!value || 'Field is required',
    exact_length: length => {
      return function (value) {
        if (value && value.length != length) {
          return 'Field should be of at least '+length+ ' characters';
        }
        return true;
      };
    },
    min_length: length => {
      return function (value) {
        if (value && value.length < length) {
          return 'Minimum length '+length+' is required';
        }
        return true;
      };
    },
    max_length: length => {
      return function (value) {
        if (value && value.length > length) {
          return  (' validation.max_length', {length: length});
        }
        return true;
      };
    },
    email: value => {
        const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(value){
          return pattern.test(value) || 'invalid email';
        }
        return true
    },
}
