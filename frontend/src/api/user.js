import * as ApiManager from './_manager'

export const User = {
  register: payload => {
     return   ApiManager.callApi( 'auth', 'create', payload) 
  },
  login: payload => {
    return   ApiManager.callApi( 'auth', 'login' , payload) 
 },
  logout: payload =>{
     return ApiManager.callApi( 'auth' , 'logout' , payload)
  }
}