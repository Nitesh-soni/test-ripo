import store from '../store'
import axios from "axios"
//const HASH_ALGO = 'sha256'
const URL = process.env.VUE_APP_API_URL

const buildPayload = (namespace, action, payload) => {
  let auth = null;
  let user = store.state.user.user ;
  if (user.secret) {
    let nonce = require ('nonce') ()
    let timestamp = '1' + nonce ()
    let secret = user.secret
    let user_id = user.id
    let signature = user_id + timestamp + secret;
    let hash = doHash (signature)
    auth = {
      'timestamp' : timestamp,
      'user_id' : user_id,
      'hash' : hash
    }
  }

  return {
    'namespace' : namespace,
    'action' : action,
    'payload' : payload,
    'auth' : auth
  }
}

const doHash = req => {
    let md5 = require('md5');
    let hash = md5(req);
    return hash;
}


const parseResponse = response => {
    // Error
    if (response.data.error) {
      throw response.data.error.message
    }
    // Data is OK
    if (response.data.status === 200) {
      return response.data.data
    }
}


async function callApi (namespace, action, payload) {     
    return axios
      .post (URL,buildPayload (namespace, action, payload))
      .then (response => {
        return parseResponse (response)
      })
      .catch (function ( error ) {
        //catch axios errors
        throw error ;
      })
}


export { URL, axios, callApi }