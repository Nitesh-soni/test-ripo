import * as ApiManager from './_manager'

export const Task = {
     create: payload => {
        return   ApiManager.callApi( 'task' ,'create' , payload) 
     },
     get: payload =>{
         return ApiManager.callApi( 'task' , 'list' , payload)
     },
     update: payload =>{
        return ApiManager.callApi( 'task' , 'update' , payload)
     },
     delete: payload =>{
        return ApiManager.callApi( 'task' , 'delete' , payload)
     }
}