import * as API from "../../api";
import DEFINES from "@/defines";
function initialState() {
  return {
    user: []
  };
}

const state = initialState();

const actions = {
  async register(context, payload) {
    let response = await API.User.register(payload);
    context.dispatch(
      "global/showMessage",
      {
        message: response.message,
        color: "green"
      },
      DEFINES.USE_ROOT
    );
    return response;
  },
  async login(context, payload) {
    let response = await API.User.login(payload);
    context.commit("set_user", {
      user: response.user
    });
    context.dispatch(
      "global/showMessage",
      {
        message: response.message,
        color: "green"
      },
      DEFINES.USE_ROOT
    ),
      //TODO:: Have to fix
      context.dispatch("task/list", {}, DEFINES.USE_ROOT);
    return response;
  }
};
const mutations = {
  set_user(state, payload) {
    state.user = payload.user;
  },
  reset(state) {
    const s = initialState();
    Object.keys(s).forEach(key => {
      state[key] = s[key];
    });
  }
};
const getters = {
  isLoggedIn(state) {
    return state.user.secret;
  }
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
