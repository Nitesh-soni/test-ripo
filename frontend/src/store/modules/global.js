import {EventBus} from '@/eventBus.js'
//import DEFINES from '@/defines.js'

function initialState () {
  return {
    
  }
}

// state
const state = initialState ()

// getters
const getters = {  
 
}

// actions
const actions = {
  showMessage (context, payload) {
  EventBus.$emit('showMessage', payload)
  },
}

// mutations
const mutations = {
    }

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
