import * as API from "../../api";
import DEFINES from "@/defines";
import router from "../../router"
function initialState() {
  return {
    list: []
  };
}

const state = initialState();

const actions = {
  async create(context, payload) {
    let response = await API.Task.create(payload);
    context.commit("list", {
      data: response.tasks
    });
    return response;
  },

  async update(context, payload) {
    let response = await API.Task.update(payload);
    context.commit("list", {
      data: response.tasks
    });
    return response;
  },

  async list(context) {
    let response = await API.Task.get();
    context.commit("list", {
      data: response.tasks
    });
    return response;
  },

  async delete(context, payload) {
    let response = await API.Task.delete(payload);
    context.dispatch(
      "global/showMessage",
      {
        message: response.message,
        color: "green"
      },
      DEFINES.USE_ROOT
    );
    context.commit("delete", {
      id: payload.id
    });
    router.push({name:'list'});
    return response;
  }
};

const getters = {
  get: state => id => {
    var item = state.list.find(item => {
      return item.id == id;
    });
    return Object.assign({},item);
  }
};

const mutations = {
  list(state, payload) {
    state.list = payload.data;
  },
  delete(state ,payload) {
    const index = state.list.findIndex(obj => obj.id === payload.id);
    state.list.splice(index, 1);
  },
  reset(state) {
    const s = initialState();
    Object.keys(s).forEach(key => {
      state[key] = s[key];
    });
  }
};
export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
