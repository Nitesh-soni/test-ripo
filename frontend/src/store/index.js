import Vue from "vue";
import Vuex from "vuex";
import user from './modules/user';
import task from './modules/task';
import global from './modules/global';
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  actions: {
    async logout ({commit}){
      commit ('user/reset');
      commit ('task/reset');
    }
  },
  modules: {
    namespaced: true,
    user,
    task,
    global
  },
  plugins: [
    createPersistedState({
      key: 'todo_list'
    })
  ]
 
});
